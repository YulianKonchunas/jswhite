
Crafty.scene('Game', function() 
{
		Crafty.e("GameField");
});

//this two functions should be inserted into separate object
function getCellCoordinate(index)
{
	return 5 + index * Math.ceil(Global.ballRadius*4.6);
}

function getBallCoordinate(index)
{
	return index * Math.ceil(Global.ballRadius*2.1);
}

function randomColor()
{
	return new HSVColour(Math.random()*360,95,90);
}
function swapBalls(firstBall,secondBall)
{
	firstBall.unbindMouse();
	var firstBallCell = firstBall._parent;
	var secondBallCell = secondBall._parent;
	
	//swap in cell's arrays
	firstBallCell.swap(firstBall,secondBall);
	secondBallCell.swap(secondBall,firstBall);
	
	//swap parentness
	//detach
	firstBallCell.detach(firstBall);
	secondBallCell.detach(secondBall);
	
	//attach
	firstBallCell.attach(secondBall);
	secondBallCell.attach(firstBall);
	
	//swap positions
	//var secondBallX = secondBall._x ;
	//var secondBallY = secondBall._y ;
	var firstBallX = firstBall._x ;
	var firstBallY = firstBall._y ;
	var secondBallX = secondBallCell._x + secondBallCell._w/2 - secondBall.w/2;
	var secondBallY = secondBallCell._y + secondBallCell._h/2 - secondBall.h/2 ;
	secondBall.tween({x:firstBallX, y:firstBallY}, 15);
	firstBall.tween({x:secondBallX, y:secondBallY}, 15);
	
	firstBall.bind("TweenEnd", function(prop)
	{
		if (prop == "y")
		{
			this.unbind("TweenEnd");
			secondBallCell.merge(firstBall);
		}
	});
	//secondBallCell.merge(firstBall);
}

function shadeColor(color, percent)
{
	var R = color.getRGB().r;
	var G = color.getRGB().g;
    var B = color.getRGB().b;

    R = parseInt(R * (100 + percent) / 100);
    G = parseInt(G * (100 + percent) / 100);
    B = parseInt(B * (100 + percent) / 100);

    R = (R<255)?R:255;  
    G = (G<255)?G:255;  
    B = (B<255)?B:255;  
	
	var returnColor = new RGBColour(R,G,B);

    // var RR = ((R.toString(16).length==1)?"0"+R.toString(16):R.toString(16));
    // var GG = ((G.toString(16).length==1)?"0"+G.toString(16):G.toString(16));
    // var BB = ((B.toString(16).length==1)?"0"+B.toString(16):B.toString(16));
    // return "#"+RR+GG+BB;
	return returnColor;
}
