
//Global.ballRadius = 25;

Crafty.c("GameField",
{	
	init: function()
	{
		this.requires("Delay");
		this.score = 0;
		this.scoreLabel = Crafty.e("2D, DOM, Text")
						  .attr({ x: Global.screenWidth-100, y: Global.screenHeight-100});
		this.scoreLabel.text("Score: "+this.score).textColor("white");
		
		this.roundsLeft = 3;
		this.roundsLabel = Crafty.e("2D, DOM, Text")
						  .attr({ x: Global.screenWidth-100, y: Global.screenHeight-200});
		this.roundsLabel.text("Rounds Left: "+this.roundsLeft).textColor("white");



		this.lastMerge = false;

		this.lastParent = null;
		this.lastParentCount = 0;
		
		var selectedBall = null;
		
		this.cells = [];
		
		for (var i = 0; i < Global.cellRowsCount; i++)
			for (var j = 0; j < Global.cellColsCount; j++)
			{
				this.cells.push(
				Crafty.e('Cell')
				.attr({
					x: getCellCoordinate(i),
					y: getCellCoordinate(j)
				})
				);
			}
		
		//test merge nearly everything
		// for (var i = this.cells.length - 1; i >= 2; i--)
		// {
		// 	this.cells[i].justMerge();
		// }



		this.bind("BallClicked", function(ball)
		{
			//console.log("BallClicked event " + ball.x + " " + ball.y);
			if (selectedBall == null)
			{
				selectedBall = ball;
			}
			else
			{
				if (selectedBall._parent != ball._parent)
				{
					this.lastParentCount = 0;
					swapBalls(selectedBall, ball);
				}
				else if (selectedBall != ball)
				{
					this.lastParentCount++;
					if (this.lastParentCount == 2)
					{
						this.lastParentCount = 0;
						ball._parent.justMerge();
					}
				}
				selectedBall.deselect();
				ball.deselect();
				selectedBall = null;
			}
		});
		this.bind("AddToScore", function(scoreToAdd)
		{
			this.score += scoreToAdd;
			this.scoreLabel.text("Score: "+this.score).textColor("white");


			if (!this.lastMerge)
			{
				//added this check here for fast
				//locates last unmerged cell
				var lastUnmerged = -1;
				for (var i = 0; i < this.cells.length; i++)
				{
					if (this.cells[i].isMerged == false)
					{
						if (lastUnmerged == -1)
						{
							lastUnmerged = i;
						}
						else
						{
							//more than one cell is unmerged
							return;
						}
					}
				}
				if (lastUnmerged != -1)
				{
					this.lastMerge = true;
					this.cells[lastUnmerged].justMerge();
				}
			}
			else
			{
				this.delay(this.diminishRandomlyAnimation, 300, 0);
				this.roundsLeft--;
				this.roundsLabel.text("Rounds Left: "+this.roundsLeft).textColor("white");
				if (this.roundsLeft == 0)
				{
					this.roundsLabel.text("That's all, folks!");
					Crafty.pause();
				}

			}
		});
	},
	diminishRandomlyAnimation: function()
	{
		var ballsRowsCount = Global.cellRowsCount * 2;
		var ballsColsCount = Global.cellColsCount * 2;
		console.log("ball rows = " + ballsRowsCount);
		console.log("ball cols = " + ballsColsCount);
		var balls = new Array(ballsRowsCount);
		for (var i = 0; i < ballsRowsCount; i++)
		{
			balls[i] = new Array(ballsColsCount);
		}

		for (var i = 0; i < this.cells.length; i++)
		{
			var multiplier = Math.round(parseInt(this.cells[i].bigBall.text)/10);
			if (multiplier > 1)
			{
				multiplier = "x"+multiplier;	

			}
			else
			{
				multiplier = "";
			}


			this.cells[i].bigBall.text = multiplier;
			this.cells[i].detach(this.cells[i].bigBall);
		}

		for (var i = 0; i < this.cells.length; i++)
		{
			var randi;
			var randj;
			do
			{
				randi = Math.floor(Math.random()*ballsRowsCount);
				randj = Math.floor(Math.random()*ballsColsCount);
			} while (balls[randi][randj]);

			console.log("randi=" + randi);
			console.log("randj=" + randj);
			balls[randi][randj] = this.cells[i].bigBall;
			this.cells[i].detach(this.cells[i].bigBall);
			balls[randi][randj].tween({
					x: getCellCoordinate(Math.floor(randi/2)) + getBallCoordinate(randi%2),
					y: getCellCoordinate(Math.floor(randj/2)) + getBallCoordinate(randj%2),
					w:Global.ballRadius*2, h:Global.ballRadius*2,
					radius: Global.ballRadius
					}, 15);
			balls[randi][randj].bindMouse();
		}
		for (var i = 0; i < ballsRowsCount; i++)
		{
			for (var j = 0; j < ballsColsCount; j++)
			{
				if (!balls[i][j])
				{
					balls[i][j] = Crafty.e('Ball')
					.Circle(Global.ballRadius/2, randomColor())
					.attr({
						x: getCellCoordinate(Math.floor(i/2)) + getBallCoordinate(i%2),
						y: getCellCoordinate(Math.floor(j/2)) + getBallCoordinate(j%2),
					})
					.tween({
						x: getCellCoordinate(Math.floor(i/2)) + getBallCoordinate(i%2),
						y: getCellCoordinate(Math.floor(j/2)) + getBallCoordinate(j%2),
						w: Global.ballRadius*2,
						h: Global.ballRadius*2,
						radius: Global.ballRadius
					}, 10);
				}
			}
		}

		var that = this;
		//group in this.cells when last ball animation finished
		balls[ballsRowsCount-1][ballsRowsCount-1].bind("TweenEnd",function(prop)
		{
			this.unbind("TweenEnd");
			for (var i = 0; i < 3; i++)
			{
				for (var j = 0; j < 3; j++)
				{
					//TODO make this less clumsy
					var index = i*3 + j;
					that.cells[index].reInit();
					that.cells[index].balls.push(balls[i*2][j*2]);
					that.cells[index].balls.push(balls[i*2+1][j*2]);
					that.cells[index].balls.push(balls[i*2][j*2+1]);
					that.cells[index].balls.push(balls[i*2+1][j*2+1]);
					
					for (var k = 0; k < that.cells[index].balls.length; k++)
					{
						that.cells[index].attach(that.cells[index].balls[k]);
					}
				}
			}
		});
		this.lastMerge = false;


	},
	diminishCorrespondinglyAnimation: function()
	{
		//animation when last merge was done
		var balls = new Array(6);
		for (var i = 0; i < 6; i++)
		{
			balls[i] = [];
		}

		for (var i = 0; i < this.cells.length; i++)
		{
			var multiplier = Math.round(parseInt(this.cells[i].bigBall.text)/100);
			if (multiplier > 1)
			{
				multiplier = "x"+multiplier;	

			}
			else
			{
				multiplier = "";
			}


			this.cells[i].bigBall.text = multiplier;
			this.cells[i].detach(this.cells[i].bigBall);
		}
		//this.cells[5].bigBall.text = "x555";

		//ensmall large balls
		for (var i = 1; i < 4; i++)
		{
			for (var j = 1; j < 4; j++)
			{
				var cellIndex = (i-1)*3+(j-1);
				balls[i][j] = this.cells[cellIndex].bigBall;
				this.cells[cellIndex].detach(this.cells[cellIndex].bigBall);
				balls[i][j].tween({
						x: getCellCoordinate(Math.floor(i/2)) + getBallCoordinate(i%2),
						y: getCellCoordinate(Math.floor(j/2)) + getBallCoordinate(j%2),
						w:Global.ballRadius*2, h:Global.ballRadius*2,
						radius: Global.ballRadius
						}, 10);
				balls[i][j].bindMouse();
				
			}
		}
		
		//place left and top
		j=0;
		for (var i = 0; i < 6; i++)
			if (!balls[i][j])
			{
				balls[i][j] = Crafty.e('Ball')
				.Circle(Global.ballRadius, randomColor())
				.attr({
					x: getCellCoordinate(Math.floor(i/2)) + getBallCoordinate(i%2),
					y: getCellCoordinate(Math.floor(j/2)) + getBallCoordinate(j%2) - Global.ballRadius*2
					});
				//.tween({y: getCellCoordinate(Math.floor(j/2)) + getBallCoordinate(j%2)},15);
			}
		i=0;			
		for (var j = 0; j < 6; j++)
		{
			if (!balls[i][j])
			{
				balls[i][j] = Crafty.e('Ball')
				.Circle(Global.ballRadius, new HSVColour(Math.random()*360,100,70))
				.attr({
					x: getCellCoordinate(Math.floor(i/2)) + getBallCoordinate(i%2) - Global.ballRadius*2,
					y: getCellCoordinate(Math.floor(j/2)) + getBallCoordinate(j%2)
					});
				//.tween({x: getCellCoordinate(Math.floor(i/2)) + getBallCoordinate(i%2)},15);
			}
			else //if corner balls
			{
				balls[i][j].x = balls[i][j]._x - Global.ballRadius*2;
			}
		}

		//place bottom and down 
		for (var i = 4; i < 6; i++)
		{
			for (var j = 0; j < 6; j++)
			{
				if (!balls[i][j])
				{
					balls[i][j] = Crafty.e('Ball')
					.Circle(Global.ballRadius, randomColor())
					.attr({
						x: getCellCoordinate(Math.floor(i/2)) + getBallCoordinate(i%2) + Global.ballRadius*2*(i-3),
						y: getCellCoordinate(Math.floor(j/2)) + getBallCoordinate(j%2)
						});
					//.tween({x: getCellCoordinate(Math.floor(i/2)) + getBallCoordinate(i%2)},10 + (i - 4) * 5);
				}
			}
		}
		for (var i = 0; i < 6; i++)
		{
			for (var j = 4; j < 6; j++)
			{
				if (!balls[i][j])
				{
					balls[i][j] = Crafty.e('Ball')
					.Circle(Global.ballRadius, randomColor())
					.attr({
						x: getCellCoordinate(Math.floor(i/2)) + getBallCoordinate(i%2),
						y: getCellCoordinate(Math.floor(j/2)) + getBallCoordinate(j%2) + Global.ballRadius*1.5*(j-3)
						});
					//.tween({y: getCellCoordinate(Math.floor(j/2)) + getBallCoordinate(j%2)},10 + (j - 4) * 5);
				}
				else
				{
					balls[i][j].y = balls[i][j]._y + Global.ballRadius*1.5*(j-3);
				}
			}
		}


		for (var i = 0; i < 6; i++)
		{
			for (var j = 0; j < 6; j++)
			{
				//here we should avoid unnecessary animationing
				//if ((i < 1 || i > 3) || (j < 1 || j > 3))
				{
					var animationTime = 10;
					if (i == 0 || j == 0 || i == 5 || j == 5)
						animationTime = 13;
					balls[i][j].tween({
							x: getCellCoordinate(Math.floor(i/2)) + getBallCoordinate(i%2),
							y: getCellCoordinate(Math.floor(j/2)) + getBallCoordinate(j%2)
							}, animationTime);
				}
			}
		}
		var that = this;
		//group in this.cells when last ball animation finished
		balls[5][5].bind("TweenEnd",function(prop)
		{
			this.unbind("TweenEnd");
			for (var i = 0; i < 3; i++)
			{
				for (var j = 0; j < 3; j++)
				{
					//TODO make this less clumsy
					var index = i*3 + j;
					that.cells[index].reInit();
					that.cells[index].balls.push(balls[i*2][j*2]);
					that.cells[index].balls.push(balls[i*2+1][j*2]);
					that.cells[index].balls.push(balls[i*2][j*2+1]);
					that.cells[index].balls.push(balls[i*2+1][j*2+1]);
					
					for (var k = 0; k < that.cells[index].balls.length; k++)
					{
						that.cells[index].attach(that.cells[index].balls[k]);
					}
				}
			}
		});
		this.lastMerge = false;
	}
});