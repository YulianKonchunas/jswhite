Global = {
	cellRowsCount: 3,
	cellColsCount: 3,
	ballRadius: 5,
	screenWidth: 800,
	screenHeight: 600,
	//finds closest difference between two numbers
	//assuming 361 and 0 are the same point
	//e.g. 20 differs from 320 for only 60, not 300
	cyclicDifference: function(arg1, arg2)
	{
		var sub = Math.abs(arg1 - arg2);
		return Math.min(sub, 360 - sub);
	}
};