Crafty.c("OutlinedCircle",
{	
	shadowBlur: 0,
	strokeStyle: "black",
	outlineMod: 1,
	selected: false,
	hueModifier: 0,
	debugColors: false,
    Circle: function(radius, color)
	{
		this.text = "";
        this.radius = radius;
        this.w = this.h = this.radius * 2 + 2;
		
		this.color = color;
		//this.bind("Move",this.move);
        return this;
    },
    
    draw: function()
	{	
		var hue = this.color.getHSV().h + this.hueModifier;
		var saturation = this.color.getHSV().s;
		var value = this.color.getHSV().v;
		this.color = new HSVColour(hue, saturation, value);
		var ctx = Crafty.canvas.context;
		ctx.save();
		//ctx.strokeStyle = this.strokeStyle;
		ctx.lineWidth = this.outlineMod;
	 
		//ctx.shadowBlur = this.shadowBlur;
		//ctx.shadowColor = this.shadowColor;
	 
		// Create gradient
		//var gradient = ctx.createRadialGradient(this.x, this.y, this.radius, this.x, this.y, this.radius*2);
		var centerX = this.x + this.w / 2;
		var centerY = this.y + this.h / 2;
		//var gradient = ctx.createRadialGradient(centerX, centerY, this.radius/8,
		//										centerX, centerY, this.radius);
		//var lightenColor = shadeColor(this.color, 100);
		//gradient.addColorStop(0, lightenColor.getCSSHexadecimalRGB());
		//.addColorStop(1, color.getCSSHexadecimalRGB());

		// Fill with gradient
		//ctx.fillStyle = gradient;
		//Fill with plain color
		ctx.fillStyle = this.color.getCSSHexadecimalRGB();

		
		if (!this.selected)
			ctx.strokeStyle = "rgba(0,  0,  0, .7)";
		else
			ctx.strokeStyle = "rgba(255,255,255, .75)";
		
		if (this.debugColors)
		{
			console.log("hueMod ", this.hueModifier);
			console.log("color: ", this.color.getCSSHexadecimalRGB());
		}
		//ctx.fillStyle = color.getCSSHexadecimalRGB();
	   
		//ctx.fillStyle = this.color;
		ctx.beginPath();
		ctx.arc(
			centerX,
			centerY,
			this.radius,
			0,
			Math.PI * 2
		);
		ctx.closePath();
		ctx.fill();
		
		ctx.beginPath();
		ctx.arc(
			centerX,
			centerY,
			this.radius - this.outlineMod*0.5,
			0,
			Math.PI * 2
		);
		ctx.closePath();
		ctx.stroke();

		if (this.text != "")
		{
				ctx.textAlign = "center";
   				ctx.textBaseline = "middle";
   				if ((hue>200 && hue<360) || (hue>0 && hue<30))
           			ctx.fillStyle = "#eeeeee";
        		else
           			 ctx.fillStyle = "#000000";	
			    ctx.font = "bold " + this.radius*0.5 + "px  Arial";

			    ctx.fillText(this.text, centerX , centerY);
		}

		ctx.restore();
    }
});

