Crafty.c("Ball",
{	
	init: function()
	{
		this.requires("2D, Canvas, Mouse, Tween, OutlinedCircle");		
		this.bindMouse();

	},
	// invertSelection: function()
	// {
	// 	if (!this.selected)
	// 		this.shadowColor = "white";
	// 	else
	// 		this.shadowColor = this.color.getCSSHexadecimalRGB();
	// 	this.selected = !this.selected;
	// 	this.draw();
	// },
	select: function()
	{
		this.selected = true;
		this.draw();	
	},
	deselect: function()
	{
		this.selected = false;
		this.draw();
	},
	bindMouse: function()
	{
		this.bind("MouseOver", function(e)
		{
			//this.tween({shadowBlur: 20, w: this.w}, 5);
			this.tween({outlineMod: 10, alpha: this.alpha}, 10);
			//Crafty.background(this.color.getCSSHexadecimalRGB());
		});
		this.bind("MouseOut", function(e)
		{
			if (!this.selected)
				//this.tween({shadowBlur: 0, w: this.w}, 5);
				this.tween({outlineMod: 1, alpha: this.alpha}, 10);
		});
		this.bind("Click", function(e)
		{
			this.select();
			Crafty.trigger('BallClicked', this);
		});
	},
	unbindMouse: function()
	{
		this.unbind("MouseOver");
		this.unbind("Click");
		this.outlineMod = 1;
		this.selected = false;
		this.draw();
	}/*,
	setText: function(textToSet)
	{
		var textLabel = Crafty.e("2D, Canvas, Text, Tween")
		.attr({z: 500});
		//.tween({y: destY*0.6, alpha: 0.0}, 150);


		this.attach(textLabel);
		textLabel.text(textToSet).textColor("#FFFFFF");
		textLabel.x = this._x + this._w/2;
		textLabel.y = this._y + this._h/2;
		console.log("textLabel width: ",textLabel.w);
	}*/
});