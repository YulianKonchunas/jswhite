
Crafty.c("Cell",
{
	init: function()
	{
		this.requires("2D");
		this.balls = [];
		
		//initialize array of four balls and set their's positions
		for (var i = 0; i < 2; i++)
		{
			for (var j = 0; j < 2; j++)
			{
				var index = i*2+j;
				this.balls[index] = Crafty.e('Ball')
				.Circle(Global.ballRadius, randomColor())
				.attr({
					x: this._x + getBallCoordinate(i),
					y: this._y + getBallCoordinate(j)
					});
				
				//set parentness, so balls will move with the cell
				this.attach(this.balls[index]);
			}
		}
		
		//calculate dimesions
		this._w = this._h = this.balls[0]._w + this.balls[1]._w; 
		
		this.bigBall = null;
		this.isMerged = false;
		
	},	

	merge: function(ballToMerge)
	{
		if (ballToMerge._parent == this)
		{
			//form the color of future ball

			//find the closest pair
			/*var mini = 0;
			var minj = 1;
			var smallestDiff = Global.cyclicDifference(this.balls[mini].color.getHSV().h,
													   this.balls[minj].color.getHSV().h);
			for (var i = 0; i < this.balls.length; i++)
			{
				for (var j = i+1; j < this.balls.length; j++)
				{
					var currentDiff = Global.cyclicDifference(this.balls[i].color.getHSV().h,
															  this.balls[j].color.getHSV().h);
					if (currentDiff < smallestDiff)
					{
						mini = i;
						minj = j;
						smallestDiff = currentDiff;
					}	
				}
			}
			console.log("smallest first = " + this.balls[mini].color.getHSV().h);
			console.log("smallest second = " + this.balls[minj].color.getHSV().h);
			console.log("differense itself = " + smallestDiff);
			var basicHue = this.balls[mini].color.getHSV().h + this.balls[minj].color.getHSV().h;
			basicHue /= 2;
			console.log("basic hue = " + basicHue);*/

			var rc = 0;
			var gc = 0;
			var bc = 0;
			
			for ( var i = 0; i < this.balls.length; i++)
			{
				rc += this.balls[i].color.getRGB().r;
				gc += this.balls[i].color.getRGB().g;
				bc += this.balls[i].color.getRGB().b;
			}
			rc/=4;
			gc/=4;
			bc/=4;
			
			var finalColor = new RGBColour(rc,gc,bc);
			//var finalColor = new HSVColour(basicHue,95,90);
			var hueMod = finalColor.getHSV().h - ballToMerge.color.getHSV().h;
			hueMod /= 10;
			console.log("hueMod " + hueMod)
			//hueMod = Math.floor(hueMod);


			var radi = this._w/2;
			

			var destX = this._x + this._w/2 - ballToMerge._w/2;
			var destY = this._y + this._h/2 - ballToMerge._h/2;
			

			ballToMerge.z = this.z + 4;
		
			ballToMerge.unbindMouse();
			//ballToMerge.debugColors = true;
			// ballToMerge.w = radi*2;
			// ballToMerge.h = radi*2;
			//ballToMerge.attr({x: this._x, y: this._y, w: this._w, h: this._h, radius: radi});
			//ballToMerge.attr({x: this._x, y: this._y});
			
			//last one is here:
			//ballToMerge.tween({radius: radi, hueModifier: hueMod}, 10);
			ballToMerge.tween({x: this._x, y: this._y, w: this._w, h: this._h, radius: radi, hueModifier: hueMod}, 10);
			ballToMerge.bind("TweenEnd", function(prop)
			{
				if (prop == "hueModifier")
				{
					this.unbind("TweenEnd");
					this.hueModifier = 0;

					var scoreToAdd = Math.floor(this._parent.computeScore(finalColor.getHSV().h));
					this.text = scoreToAdd;
					Crafty.trigger("AddToScore", scoreToAdd);
				}
			});
			
			for ( var i = 0; i < this.balls.length; i++)
			{
				if (ballToMerge != this.balls[i])
				{
					this.balls[i].tween({x: destX, y: destY}, 10);
					this.balls[i].bind("TweenEnd", function(prop)
					{
						this.unbind("TweenEnd");
						this.visible = false;
					});
						
				}
			}
			this.bigBall = ballToMerge;
			this.isMerged = true;
			/*this.bind("ComputeScore", function(data)
			{
				var scoreToAdd = Math.floor(this.computeScore(finalColor.getHSV().h));
				Crafty.e("2D, Canvas, Text, Tween")
				.attr({ x: destX, y: destY, alpha: 1.0 })
				.text(scoreToAdd)
				.tween({y: destY*0.6, alpha: 0.1}, 10);
			});*/
		}
	},
	justMerge: function()
	{
		this.merge(this.balls[0]);
	},
	swap: function(ballToDelete, ballToInsert)
	{
		for ( var i = 0; i < this.balls.length; i++ )
		{
			if (ballToDelete == this.balls[i])
			{
				this.balls.splice(i,1,ballToInsert);
				break;
			}
		}
	},
	computeScore: function(newHue)
	{
		var scoreToAdd = 0;
		var max = -1;
		/*console.log("points scoring! nuwHue =" + newHue);
		for (var i = 0; i < 4; i++)
		{
			var cmp = Global.cyclicDifference(newHue, this.balls[i].color.getHSV().h);
			scoreToAdd += (180 - cmp);
			console.log("color of ball = " + this.balls[i].color.getHSV().h);
			console.log("it gave us " + (180 - cmp) + " points");			
		}*/

		for (var i = 0; i < this.balls.length; i++)
		{
			console.log("ball["+i+"].color.hue = " + this.balls[i].color.getHSV().h);
		}

		for (var i = 0; i < 3; i++)
		{
			var cmp = Global.cyclicDifference(this.balls[i].color.getHSV().h, this.balls[i+1].color.getHSV().h);
			scoreToAdd += (180 - cmp);
		}

		var lastAndFirst = 180 - Global.cyclicDifference(this.balls[0].color.getHSV().h, this.balls[3].color.getHSV().h);
		var diagonal1 = 180 -Global.cyclicDifference(this.balls[0].color.getHSV().h, this.balls[2].color.getHSV().h);
		var diagonal2 = 180 - Global.cyclicDifference(this.balls[1].color.getHSV().h, this.balls[3].color.getHSV().h);
		scoreToAdd += lastAndFirst + diagonal1 + diagonal2;

		//I honestly do not know where this spare 360 came from, but it is safe to subtract it
		scoreToAdd -= 360;


		//scoreToAdd = Math.round(scoreToAdd/25);
		scoreToAdd *= scoreToAdd;
		scoreToAdd = Math.ceil(scoreToAdd/10000);


		//scoreToAdd -= 720;
		//scoreToAdd *= scoreToAdd;

		//scoreToAdd = 4 * 360 - scoreToAdd;
		//scoreToAdd = 720 - scoreToAdd;

		// var scoreString = "";
		/*for (var j = 0; j < 10000; j++)
		{
			scoreToAdd = 0;
			newHue = Math.random()*360;
			for (var i = 0; i < 4; i++)
			{	
				var cmp = Global.cyclicDifference(newHue, Math.random()*360);
				scoreToAdd += (180 - cmp);
			}
			if (max == -1)
				max = scoreToAdd
			if (max > scoreToAdd)
				max = scoreToAdd;
		}
		console.log("min: " + max);*/

		/*for (var j = 0; j < 100; j++)
		{

			scoreToAdd = 0;
			for (var i = 0; i < this.balls.length; i++)
			{
				this.balls[i].color = randomColor();
			}			

			for (var i = 0; i < 3; i++)
			{
				var cmp = Global.cyclicDifference(this.balls[i].color.getHSV().h, this.balls[i+1].color.getHSV().h);
				scoreToAdd += (180 - cmp);
			}

			var lastAndFirst = 180 - Global.cyclicDifference(this.balls[3].color.getHSV().h, this.balls[0].color.getHSV().h);
			var diagonal1 = 180 - Global.cyclicDifference(this.balls[0].color.getHSV().h, this.balls[2].color.getHSV().h);
			var diagonal2 = 180 - Global.cyclicDifference(this.balls[1].color.getHSV().h, this.balls[3].color.getHSV().h);
			scoreToAdd += lastAndFirst + diagonal1 + diagonal2;

			console.log("lastAndFirst: " + lastAndFirst);
			console.log("diagonal1: " + diagonal1);
			console.log("diagonal2: " + diagonal2);
			//scoreToAdd /= 25;

			if (max == -1)
				max = scoreToAdd
			if (max > scoreToAdd)
				max = scoreToAdd;
		}

		console.log("min: " + max);*/
		

		//multiply score
		var multiplier = 0;
		for (var i = 0; i < this.balls.length; i++)
		{
			if (this.balls[i].text != "")
			{
				var currentMul = parseInt(this.balls[i].text.substr(1,this.balls[i].text.length));
				console.log("multiplier[" + i + "] = " + currentMul);
				multiplier += currentMul;
			}
		}
		if (multiplier>0)
			scoreToAdd *= multiplier;

		return scoreToAdd;
	},
	// old
	/*computeScore: function(newHue)
	{
		var max = -1;
		for (var i = 0; i < 4; i++)
		{
			var cmp = Global.cyclicDifference(newHue, this.balls[i].color.getHSV().h);
			if (max == -1)
			{
				max = cmp;
				continue;
			}
			if (cmp > max) max = cmp;
		}
		var scoreToAdd = 0;
		if (max <= 2)
		{
			//time+=15;
			scoreToAdd = 300;
		}
		else if (max <= 50)
		{
			max = 51-max;
			scoreToAdd = max * Math.ceil(max/10) + 11;
		}
		else
		{
			max = Math.ceil((180-max)/10);
			scoreToAdd = max;
		}
		return scoreToAdd;
	},*/
	reInit: function()
	{
		if (this._children)
			for (var i = 0; i < this._children.length; i++)
			{
				this.detach(this._children[i]);
			}
		this.balls = [];
		this.bigBall = null;
		this.isMerged = false;
	}
});