Game = {
// Initialize and start our game
	start: function()
	{
		// Start crafty and set a background color so that we can see it's working
		Global.screenWidth = 800;
		Global.screenHeight = 600;

		Global.ballRadius = Math.floor(Global.screenWidth/20);

		Crafty.init(Global.screenWidth, Global.screenHeight);
		Crafty.background("gray");
		Crafty.scene("Game");

	}
}
